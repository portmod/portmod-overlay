# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

MY_PV=${PV/_pre/-pre-release-}

DESCRIPTION="A tool to examine and manipulate plugins for the Elder Scrolls game Morrowind"
HOMEPAGE="https://github.com/john-moonsugar/tes3cmd"
SRC_URI="https://github.com/john-moonsugar/$PN/archive/v$MY_PV.tar.gz"
S="$WORKDIR/$PN-$MY_PV"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="dev-lang/perl"

src_install() {
	dobin tes3cmd
	dodoc ChangeLog
}

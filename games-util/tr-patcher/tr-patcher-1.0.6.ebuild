# Copyright 2020-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit java-pkg-2 java-pkg-simple

DESCRIPTION="A modified, CLI version of the tr-patcher by the Tamriel Rebuilt Team."
HOMEPAGE="https://gitlab.com/bmwinger/tr-patcher"
SRC_URI="https://gitlab.com/bmwinger/$PN/-/archive/v$PV/$PN-v$PV.tar.bz2 -> $P.tar.bz2"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"

# Some build options will be incompatible with gradle 9 according to a warning
DEPEND="<dev-java/gradle-bin-9"
RDEPEND="${DEPEND}"

S="$WORKDIR/$PN-v$PV"

RESTRICT="network-sandbox"

src_compile() {
	TERM=xterm gradle build -g "$T" || die
}

src_install() {
	sed -i "s/gradle build//g" install.sh
	./install.sh "$D/usr" || die
}

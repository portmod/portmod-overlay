# Copyright 2019-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{8,9,10,11,12} )
DISTUTILS_USE_PEP517="setuptools"

inherit git-r3 distutils-r1

DESCRIPTION="A CLI tool to manage mods for OpenMW"
HOMEPAGE="https://gitlab.com/portmod/importmod"
EGIT_REPO_URI="https://gitlab.com/portmod/importmod.git"

LICENSE="GPL-3"
SLOT="0"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"
IUSE="test openmw"

PYTHON_REQ_USE="ssl,threads"
distutils_enable_tests pytest

RDEPEND="${PYTHON_DEPS}
	>=games-util/portmod-2.0
	dev-python/gitpython[${PYTHON_USEDEP}]
	dev-python/requests[${PYTHON_USEDEP}]
	app-arch/patool[${PYTHON_USEDEP}]
	dev-python/redbaron[${PYTHON_USEDEP}]
	dev-python/isort[${PYTHON_USEDEP}]
	dev-python/beautifulsoup4[${PYTHON_USEDEP}]
	dev-python/black[${PYTHON_USEDEP}]
	openmw? (
		games-util/tr-patcher
		games-util/tes3cmd
	)
	dev-python/pygithub[${PYTHON_USEDEP}]
	dev-vcs/python-gitlab[${PYTHON_USEDEP}]
	dev-python/ruamel-yaml[${PYTHON_USEDEP}]
	dev-python/pypi-simple[${PYTHON_USEDEP}]
	dev-python/virtualenv[${PYTHON_USEDEP}]
	dev-python/tomlkit[${PYTHON_USEDEP}]
	dev-python/prompt-toolkit[${PYTHON_USEDEP}]

"
DEPEND="${RDEPEND}
	test? (
		dev-python/pytest[${PYTHON_USEDEP}]
	)
"

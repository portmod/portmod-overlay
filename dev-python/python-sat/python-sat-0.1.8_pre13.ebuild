# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{8,9,10,11,12,13,13t} )
DISTUTILS_USE_PEP517="setuptools"

inherit pypi distutils-r1

MY_PV=${PV/_pre/.dev}
MY_P=${P/_pre/.dev}

DESCRIPTION="A toolkit for SAT-based prototyping in Python"
HOMEPAGE="https://github.com/pysathq/pysat"
SRC_URI="$(pypi_sdist_url --no-normalize ${PN} ${MY_PV})"
S=$WORKDIR/$MY_P
LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="${PYTHON_DEPS}
	sys-libs/zlib
"
DEPEND="${DEPEND}
	dev-python/six
"

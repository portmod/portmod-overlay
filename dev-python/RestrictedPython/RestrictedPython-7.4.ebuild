# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{8,9,10,11,12,13,13t} )
DISTUTILS_USE_PEP517="setuptools"

inherit pypi distutils-r1

DESCRIPTION="A subset of Python allowing program input into a trusted environment."
HOMEPAGE="
	https://github.com/zopefoundation/RestrictedPython
	https://pypi.org/project/RestrictedPython/
"
MY_P="restrictedpython-${PV}"
SRC_URI="$(pypi_sdist_url --no-normalize restrictedpython ${PV})"
S="${WORKDIR}/${MY_P}"

LICENSE="ZPL"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux"
IUSE="test"

RDEPEND="${PYTHON_DEPS}"
DEPEND="${REDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]
	test? (
		dev-python/pytest[${PYTHON_USEDEP}]
		dev-python/pytest-mock[${PYTHON_USEDEP}]
	)"

RESTRICT="!test? ( test )"

python_test() {
	pytest -v -v || die
}

Title: GURU repository dependency added
Author: Benjamin Winger <bmw@disroot.org>
Posted: 2022-05-24
Revision: 1
News-Item-Format: 2.0

The guru repository has been added as a master repository to provide certain
dependencies (specifically dev-python/sphinx-argparse).

https://wiki.gentoo.org/wiki/Project:GURU

See https://wiki.gentoo.org/wiki/Project:GURU/Information_for_End_Users for
details of how to add the repository.
